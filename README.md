This Task was provided by CrossLend, bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Crosslend Coding Challenge
## Installation

Install the dependencies

### `npm i`

## Available Scripts

git clone https://formanite_200@bitbucket.org/formanite_200/crosslend-coding-challenge.git

cd crosslend-coding-challenge

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

 


